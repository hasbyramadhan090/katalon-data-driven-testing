<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Demo account                           _56eb59</name>
   <tag></tag>
   <elementGuidId>98013daf-bc3f-450d-9603-d2193b86a05d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='login']/div/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-sm-offset-3.col-sm-6</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9cfdca01-d06d-43af-a695-eada44e84143</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-sm-offset-3 col-sm-6</value>
      <webElementGuid>804de688-e7c8-4794-aa3e-cdb39b3a87fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                
            </value>
      <webElementGuid>12549bdd-003b-4fbd-9b19-2e980dad1e12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-offset-3 col-sm-6&quot;]</value>
      <webElementGuid>a924735d-6b24-4d49-a8ed-bd5abebbe623</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='login']/div/div/div[2]</value>
      <webElementGuid>fa05dc00-5d8f-4a97-8232-8fd1976559e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/following::div[1]</value>
      <webElementGuid>ebdf8172-1525-4a0b-8f49-8ffe73dcae57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::div[4]</value>
      <webElementGuid>5e4fe61e-eff5-4624-9a57-96c67090ec46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]</value>
      <webElementGuid>09b36fcb-3b80-4655-8c2f-6c2d2501f360</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                
            ' or . = '
                
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                
            ')]</value>
      <webElementGuid>edf827e5-1ffe-4904-ac35-7e4810544e30</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
